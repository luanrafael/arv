(function(window){


	var arvore = [
		
		{
			name: 'Z - Interface',
			trigger: "alerta '$INTERFACE'"
		},
		{
			name: 'X - Pesquisa',
			trigger: "alerta '$PESQUISA'"
		},
		{
			name: 'C - ERP',
			trigger: "alerta '$ERP'"
		},
		{
			name: 'V - Histórico',
			trigger: "alerta '$HISTORICO'"
		},
		{
			name: 'B - Outros',
			trigger: [

				{
					name: '1 - Planilhas',
					trigger: [
						{
							name: '1 - Prospecção 2016',
							trigger: "sis 'start &quot;C:\\DOCS CM\\Banco de Dados\\Prospecção 2016.xls&quot; &'"
						},
						{
							name: '2 - Comparação Legislação',
							trigger: "sis 'start &quot;C:\\DOCS CM\\ANS\\Comparação da Legislação.xlsx&quot; &'"
						},
						{
							name: '3 - RN',
							trigger: "sis 'start &quot;C:\\DOCS CM\\ANS\\RN 388.xlsx&quot; &'"
						},
						{
							name: '4 - Drive',
							trigger: "sis 'start &quot;https://drive.google.com/open?id=1Yu4WmjMJDtWLrioWP2y44o_C2zLPMLT97xbibfYcN38&quot; &'"
						},
					]
				},
				{
					name: '2 - Propostas',
					trigger: [
						{
							name: '1 - Proposta Comercial Pelna Saude',
							trigger: "sis 'start &quot;C:\\DOCS CM\\Plena Saude\\Proposta Comercial Plena Saude - 2016 03.docx&quot; &'"
						},
						{
							name: '2 - Modelo de Proposta',
							trigger: "sis 'start &quot;C:\\DOCS CM\\Modelos de Documentos\\Modelo de Proposta Comercial.docx&quot; &'"
						},
					]
				},
				{
					name: '3 - PPTS',
					trigger: [
						{
							name: '1 - M2G Soluções de Atendimento',
							trigger: "sis 'start &quot;C:\\DOCS CM\\Apresentacoes\\M2G Solucoes de Atendimento para Operadora de Saude.pptx&quot; &'"
						},
						{
							name: '2 - Apresentação online',
							trigger: "sis 'start &quot;http://www.integracaom2g.com.br/WebForm/webformm2gcomercial/&quot; &'"
						},
					]
				},
				{
					name: '4 - Programas',
					trigger: [
						{
							name: '1 - Skype',
							trigger: 'start `skype &`'
						},
						{
							name: '3 - Google Drive',
							trigger: "ieabreh 'https://drive.google.com/drive/?tab=mo&rfd=1#' 'Google Drive' handlerGoogleDrive"
						},
						{
							name: '4 - Bizage',
							trigger: "ieabreh 'https://drive.google.com/drive/?tab=mo&rfd=1#' 'Google Drive' handlerGoogleDrive"
						},
					]
				},
				{
					name: '5 - Email',
					trigger: [
						{
							name: '1 - email 1',
							trigger: 'alerta `email 1`'
						},
						{
							name: '2 - email 2',
							trigger: 'alerta `email 2`'
						},
					]
				}

			]
		},

	]

	window.arv = arvore;

})(window)