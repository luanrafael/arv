O arquivo jarv.js é capaz de criar um menu do SuperClient usando html, para isso ele recebe como parâmetro um array de jsons, onde cada elemento deste array define um menu desta arvore.

## Estrutura do json

```javascript
var arvore = [
{
    "name": "Menu 1",
    "trigger": "alerta 'menu 1'"
},
{
    "name": "Menu 2",
    "trigger": "alerta 'menu 2'"
}

]

var menu = Arv.create(arvore,'<ul>');
```

![x1.png](https://bitbucket.org/repo/eBddLx/images/3389330080-x1.png)

A Estrutura básica de um menu é a seguinte:


```javascript
{
    "name": "Menu 1",
    "trigger": "alerta 'menu 1'"
},
```

### Name: 
Nome do menu

### Trigger: 
Este atributo pode ser uma string que define um comando a ser executado ou pode ser um array significando que este menu tem submenus ( filhos )

Ex.
```javascript
var arvore = [
		{
		    "name": "Menu 1",
		    "trigger": "alerta 'menu 1'",
		    "shortcut": "A"
		},
		{
		    "name": "Menu 2",
		    "trigger": [
		    	{
		    		"name": "Menu 2.1",
		    		"trigger": "alerta 'menu 2.1'",
		    		"shortcut": "B"
		    	},
		    	{
		    		"name": "Menu 2.2",
		    		"trigger": [
		    			{
		    				"name": "Menu 2.2.1",
		    				"trigger": "alerta 'menu 2.2.1'"
		    			}
		    		]
		    	},
		    ]
		}

	]
```

![x2.png](https://bitbucket.org/repo/eBddLx/images/201529801-x2.png)

###shortcut:
Este atributo deve ser uma letra ou número e serve para definir a tecla de atalho daquele menu. Quando não informado a tecla de atalho é composta pela primeira letra do nome do menu

Ex.
```javascript
var arvore = [
		{
		    "name": "Menu 1",
		    "trigger": "alerta 'menu 1'",
		    "shortcut": "A"
		},
		{
		    "name": "Menu 2",
		    "trigger": "alerta 'menu 2'"
		}

	]
```

No exemplo acima, os dois menus teriam como tecla de atalho a letra “M” para mudar a tecla de atalho do “Menu 1” informei o atributo “shortcut” passando a letra “A” como parâmetro para que ela seja usada no lugar da letra “M”, assim a letra “A” aciona o “Menu 1” e a letra “M” aciona o “Menu 2”


O arquivo jarv.js é capaz de criar um menu do SuperClient usando html, para isso ele recebe como parâmetro um array de jsons, onde cada elemento deste array define um menu desta arvore.

## Estrutura do json

```javascript
var arvore = [
{
    "name": "Menu 1",
    "trigger": "alerta 'menu 1'"
},
{
    "name": "Menu 2",
    "trigger": "alerta 'menu 2'"
}

]

var menu = Arv.create(arvore,'<ul>');
```

![x1.png](https://bitbucket.org/repo/eBddLx/images/3389330080-x1.png)

A Estrutura básica de um menu é a seguinte:


```javascript
{
    "name": "Menu 1",
    "trigger": "alerta 'menu 1'"
},
```

### Name: 
Nome do menu

### Trigger: 
Este atributo pode ser uma string que define um comando a ser executado ou pode ser um array significando que este menu tem submenus ( filhos )

Ex.
```javascript
var arvore = [
		{
		    "name": "Menu 1",
		    "trigger": "alerta 'menu 1'",
		    "shortcut": "A"
		},
		{
		    "name": "Menu 2",
		    "trigger": [
		    	{
		    		"name": "Menu 2.1",
		    		"trigger": "alerta 'menu 2.1'",
		    		"shortcut": "B"
		    	},
		    	{
		    		"name": "Menu 2.2",
		    		"trigger": [
		    			{
		    				"name": "Menu 2.2.1",
		    				"trigger": "alerta 'menu 2.2.1'"
		    			}
		    		]
		    	},
		    ]
		}

	]
```

![x2.png](https://bitbucket.org/repo/eBddLx/images/201529801-x2.png)

###shortcut:
Este atributo deve ser uma letra ou número e serve para definir a tecla de atalho daquele menu. Quando não informado a tecla de atalho é composta pela primeira letra do nome do menu

Ex.
```javascript
var arvore = [
		{
		    "name": "Menu 1",
		    "trigger": "alerta 'menu 1'",
		    "shortcut": "A"
		},
		{
		    "name": "Menu 2",
		    "trigger": "alerta 'menu 2'"
		}

	]
```

No exemplo acima, os dois menus teriam como tecla de atalho a letra “M” para mudar a tecla de atalho do “Menu 1” informei o atributo “shortcut” passando a letra “A” como parâmetro para que ela seja usada no lugar da letra “M”, assim a letra “A” aciona o “Menu 1” e a letra “M” aciona o “Menu 2”



## Estrutura Html 


```html
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="main.css">
  <script type="text/javascript" src="jarv.js"></script>
  <script src="mousetrap.min.js"></script>
</head>
<body>

<div class="wrapper">
  <nav id="arv"> <!-- #Elemento onde o menu será anexado! -->
  </nav>
</div>

</body>
</html>
```