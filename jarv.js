(function(window){
	
	var Arv = function () {

		return {

			ul: '<ul>',
			li: '',
			submenu: '',
			arv: '',
			keys: [
				'trigger',
				'img',
				'name',
				'i',
				's',
			],

			create: function(jarv, options){

				this.li = this._create_li();
				this.submenu = this._create_submenu();
				
				this.init();
				this.add_ul();
				this.make(jarv);
				this.add_tag('</ul>');
				return this.arv;

			},

			make: function (jarv) {

				for(var i = 0; i < jarv.length; i++){
					
					var menu = jarv[i];
					menu['i'] = i+1;

					typeof menu.trigger == 'object' ? this.add_submenu(menu) : this.add_li(menu);

					if(typeof menu.trigger == 'object') {
						this.add_tag('<ul class="nav">');
						this.make(menu.trigger);
						this.add_tag('</ul></li>');
					}

				}

			},

			init: function(){
				this.arv = '';
			},

			add_li: function (l) {
				this.add_tag(this.replace_all(this.li, l));
			},

			add_ul: function(){
				this.add_tag(this.ul);
			},

			add_tag: function (tag) {
				this.arv += tag;
			},

			add_submenu: function(s){
				this.add_tag(this.replace_all(this.submenu, s))	;
			},

			replace_all: function(s,m){
				var ss = s;
				m['s'] = (m.shortcut || m.name[0]).toUpperCase();
				for(var k = 0; k < this.keys.length; k++)
					ss = ss.replace('{{' + this.keys[k] + '}}', m[this.keys[k]] || '');
				return ss;

			},

			_create_li: function(class_list) {

				var classes = '';
				if(class_list){
					
					for (var i = class_list.length - 1; i >= 0; i--) {
						classes += ' ' + class_list[i];
					};
				}

				return '<li data-trigger="{{trigger}}" data-img="{{img}}" data-shortchut="{{s}}" class="action '+ classes +'"> <a href="#"> {{name}} </a> </li>';

			},

			_create_submenu: function(class_list) {

				var classes = '';
				if(class_list){
					
					for (var i = class_list.length - 1; i >= 0; i--) {
						classes += ' ' + class_list[i];
					};
				}

				return '<li data-trigger="submenu" data-img="{{img}}" data-shortchut="{{s}}" class="submenu '+ classes +'"> <i class="material-icons"></i> <a href="#"> {{name}} </a> ';
			}

		}
	}
	
	window.Arv = new Arv();


})(window);